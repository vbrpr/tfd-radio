Config = {}

-- Display debug messages? This is *VERY* verbose and should only be used when needed. (true/false)
-- Do *NOT* enable unless you know what you are doing.
Config.Debug = false

Config.Framework = {
  Core = "vbrp-core", -- Compatible with qb-core or vbrp-core
  Inventory = "vbrp-inventory", -- Compatible with qb-inventory or vbrp-inventory
  Voice = "vbrp-voice", -- Accepts pma-voice or vbrp-voice
  VoiceResourceName = "pma-voice", -- The name of the resource for the voice system
}

-- Range is based upon the distance of the listener from the sender for non-repeated channels
Config.Range = {
  Min = 900.0, -- Inside this will be clear radio, between this anx Max will be increasing static the further out you go
  Max = 1700.0, -- The furthest distance where you can still hear voice, beyond that will just be garbled mess
  MutedMax = 2300.0, -- How far out to hear garbled mess, beyond this the player will hear nothing at all
}

-- Frequencies here are listed in MHz*1000 (462.5625 = 462562). Civ frequencies utilize the FRS/GMRS defined frequencies that in the US do not require a license to use.
-- These can be anything, however. If you use another radio script, make sure these do not conflict with the channels set by that script.
-- Setting Reapted to `true` means the range submixes will not be applied.
-- {
--   Frequency = "462562", -- Frequency in MHz*1000
--   Name = "1", -- Name of the channel, displayed on the radio UI (civ radios have smaller screens) and also used for the UI in vbrp-voice as `[Radio] Channel (name)`
--   Repeated = true, -- If true, the range submixes will not be applied to this channel. Default is false.
-- }
Config.Channels = {
  ["civ"] = {
    { Frequency = "462562", Name = "1" },
    { Frequency = "462587", Name = "2" },
    { Frequency = "462612", Name = "3" },
    { Frequency = "462637", Name = "4" },
    { Frequency = "462662", Name = "5" },
    { Frequency = "462687", Name = "6" },
    { Frequency = "462712", Name = "7" },
    { Frequency = "467562", Name = "8" },
    { Frequency = "467587", Name = "9" },
    { Frequency = "467612", Name = "10" },
    { Frequency = "467637", Name = "11" },
    { Frequency = "467662", Name = "12" },
    { Frequency = "467687", Name = "13" },
    { Frequency = "467712", Name = "14" },
    { Frequency = "462550", Name = "15" },
    { Frequency = "462575", Name = "16" },
    { Frequency = "462600", Name = "17" },
    { Frequency = "462625", Name = "18" },
    { Frequency = "462650", Name = "19" },
    { Frequency = "462675", Name = "20" },
    { Frequency = "462700", Name = "21" },
    { Frequency = "462725", Name = "22" },
  },
  ["ps"] = {
    { Frequency = "460325", Name = "1 PRI DISP", Repeated = true },
    { Frequency = "460500", Name = "2 TALK ARND", Repeated = true },
    { Frequency = "484112", Name = "3E SAST TAC", Repeated = true },
    { Frequency = "154905", Name = "4E EMS TAC", Repeated = true },
    { Frequency = "460575", Name = "5E SCENE 1 TAC", Repeated = true },
    { Frequency = "465612", Name = "6E SCENE 2 TAC", Repeated = true }
  }
}

-- {
--   Item = "walkie", -- The item name in the inventory
--   Channels = Config.Channels["civ"], -- The channels table to use for this radio, defined above
--   NUIRadio = "civ", -- The name of the radio to use for the NUI UI (must be 'civ' or 'ps')
--   MicClicks = {"audio_on", "audio_off"}, -- The names of the audio files to play when the mic is turned on/off (from ui directory)
-- }
Config.Radios = {
  {
    Item = "walkie",
    Channels = Config.Channels["civ"],
    NUIRadio = "civ",
    MicClicks = {"civ_talk_permit.ogg", ""}, -- Only supported by vbrp-voice
  },
  {
    Item = "floaterola",
    Channels = Config.Channels["ps"],
    NUIRadio = "ps",
    MicClicks = {"pd_talk_permit.ogg", "pd_talk_permit_end.ogg"}, -- Only supported by vbrp-voice
  }
}
