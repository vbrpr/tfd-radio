Ranges = {}
local playerTalking = {}

local filters = {
  ["default"] = {
    { name = "freq_low", value = 100.0 },
    { name = "freq_hi", value = 5000.0 },
    { name = "rm_mod_freq", value = 300.0 },
    { name = "rm_mix", value = 0.1 },
    { name = "fudge", value = 4.0 },
    { name = "o_freq_lo", value = 300.0 },
    { name = "o_freq_hi", value = 5000.0 },
  },
  ["min"] = {
    { name = "freq_low", value = 100.0 },
    { name = "freq_hi", value = 5000.0 },
    { name = "rm_mod_freq", value = 300.0 },
    { name = "rm_mix", value = 0.1 },
    { name = "fudge", value = 4.0 },
    { name = "o_freq_lo", value = 300.0 },
    { name = "o_freq_hi", value = 5000.0 },
  },
  ["step1"] = {
    { name = "freq_low", value = 100.0 },
    { name = "freq_hi", value = 5000.0 },
    { name = "rm_mod_freq", value = 300.0 },
    { name = "rm_mix", value = 0.5 },
    { name = "fudge", value = 11.0 },
    { name = "o_freq_lo", value = 300.0 },
    { name = "o_freq_hi", value = 5000.0 },
  },
  ["step2"] = {
    { name = "freq_low", value = 100.0 },
    { name = "freq_hi", value = 5000.0 },
    { name = "rm_mod_freq", value = 300.0 },
    { name = "rm_mix", value = 0.8 },
    { name = "fudge", value = 17.0 },
    { name = "o_freq_lo", value = 300.0 },
    { name = "o_freq_hi", value = 5000.0 },
  },
  ["max"] = {
    { name = "freq_low", value = 100.0 },
    { name = "freq_hi", value = 5000.0 },
    { name = "rm_mod_freq", value = 1500.0 },
    { name = "rm_mix", value = 1.3 },
    { name = "fudge", value = 25.0 },
    { name = "o_freq_lo", value = 300.0 },
    { name = "o_freq_hi", value = 5000.0 },
  }
}

-- Calculate ranges
Citizen.CreateThread(function()
  initSubmixes()
end)

function initSubmixes()
  local diff = Config.Range.Max - Config.Range.Min
  local step = diff/2
  local step1 = Config.Range.Min + step
  local step2 = step1 + step

  Ranges['default'] = {
    submix = createSubmix("radio_default", filters['default'], false),
  }
  Ranges['min'] = {
    submix = createSubmix("radio_min", filters['min'], false),
    Min = Config.Range.Min,
    Max = step1,
  }
  Ranges['step1'] = {
    submix = createSubmix("radio_step1", filters['step1'], false),
    Min = step1,
    Max = step2,
  }
  Ranges['step2'] = {
    submix = createSubmix("radio_step2", filters['step2'], false),
    Min = step2,
    Max = Config.Range.Max,
  }
  Ranges['max'] = {
    submix = createSubmix("radio_max", filters['max'], false),
    Min = Config.Range.Max,
    Max = Config.Range.MutedMax,
  }
  Ranges['mute'] = {
    submix = createSubmix("radio_mute", filters['default'], true),
  }
end

function createSubmix(name, effects, mute)
  PrintDebug("Creating submix: " .. name)
  local submix = CreateAudioSubmix(name)
  SetAudioSubmixEffectRadioFx(submix, 0.0)
  SetAudioSubmixEffectParamInt(submix, 0, 'default', 1)

  for i=1, #effects do
    PrintDebug("Setting effect: " .. effects[i].name .. " to " .. effects[i].value)
    SetAudioSubmixEffectParamInt(submix, 0, GetHashKey(effects[i].name), effects[i].value)
  end

  if not mute then
    SetAudioSubmixOutputVolumes(submix, 0, 0.25, 1.0, 0.0, 0.0, 1.0, 1.0)
  else
    SetAudioSubmixOutputVolumes(submix, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
  end
  AddAudioSubmixOutput(submix, 0)
  PrintDebug("Created submix: " .. name)
  return submix
end

function getEffect(frequency, id)
  local freq = findFrequency(frequency)

  -- If frequency doesn't exist, it might be a different radio script,
  -- or if it's repeated... set to default submix
  if not freq or freq.Repeated then
    PrintDebug("Frequency " .. frequency .. " not found or is repeated, using default submix")
    return Ranges["default"].submix
  end

  local p = promise.new()
  Framework.Functions.TriggerCallback("tfd-radio:server:getPlayerCoords", function(coords)
    p:resolve(coords)
  end, id)
  local coords = Citizen.Await(p)
  local range = #(GetEntityCoords(PlayerPedId()) - coords)
  PrintDebug("Distance from talker: " .. range .. " Frequency: " .. frequency)
  local submixToUse = nil
  local mute = false
  for _, data in pairs(Ranges) do
    if data.Min and data.Max then
      if range >= data.Min and range <= data.Max then
        submixToUse = data.submix
      end
    else
      submixToUse = data.submix
    end
  end

  if submixToUse == nil then
    submixToUse = Ranges["default"].submix
  end

  if mute or range >= Ranges['max'].Max then
    PrintDebug("Range is marked as mute or the range exceeds the max, using mute submix")
    submixToUse = Ranges["mute"].submix
  end

  return submixToUse
end

function checkLoop(frequency, id)
  Citizen.CreateThread(function()
    while playerTalking[id] do
      local effect = getEffect(id)
      MumbleSetSubmixForServerId(id, effect)
      Citizen.Wait(1000)
    end

    playerTalking[id] = nil
  end)
end

RegisterNetEvent("pma-voice:setTalkingOnRadio", function(player, talking)
  PrintDebug("Player " .. player .. " is talking: " .. tostring(talking))
  playerTalking[player] = talking
  if not talking and playerTalking[player] then
    MumbleSetSubmixForServerId(player, -1)
    return
  end

  local effect = getEffect(PlayerRadioData.frequency, player)
  Wait(100)
  MumbleSetSubmixForServerId(player, effect)
  checkLoop(PlayerRadioData.frequency, player)
end)

function checkBlock()
  local freq = findFrequency(PlayerRadioData.frequency)

  -- Non-repeated frequencies don't know if someone is talking
  if not freq.Repeated then
    return false
  end

  for _, v in pairs(playerTalking) do
    if v then
      return true
    end
  end
  return false
end

local wasBlocked = false

AddEventHandler("pma-voice:radioActive", function(radioTalking)
  if radioTalking then
    if checkBlock() then
      PrintDebug("Radio is talking and player is talking, playing block tone")
      SendNUIMessage({
        data = "TALK_DENY",
        state = true
      })
      wasBlocked = true
    else
      PrintDebug("Playing Talk Permit")
      SendNUIMessage({
        data = "PLAY_SOUND",
        sound = PlayerRadioData.micClicks[1],
        volume = 0.5,
        loop = false,
      })
    end
  elseif not radioTalking then
    if wasBlocked then
      PrintDebug("Player is not talking, stopping block tone")
      SendNUIMessage({
        data = "TALK_DENY",
        state = false
      })
    else
      PrintDebug("Playing Talk Permit End")
      SendNUIMessage({
        data = "PLAY_SOUND",
        sound = PlayerRadioData.micClicks[2],
        volume = 0.5,
        loop = false,
      })
    end
  end
end)