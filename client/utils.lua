function LoadAnimDict(dict)
  if not HasAnimDictLoaded(dict) then
    RequestAnimDict(dict)
    while not HasAnimDictLoaded(dict) do
      Citizen.Wait(0)
    end
  end
end

local radioProp = nil

function toggleAnimation(state)
  PrintDebug("Toggling animation: " .. tostring(state))
  LoadAnimDict("cellphone@")
  if state then
    TaskPlayAnim(PlayerPedId(), "cellphone@", "cellphone_text_read_base", 2.0, 3.0, -1, 49, 0, 0, 0, 0)
    radioProp = CreateObject(`prop_cs_hand_radio`, 1.0, 1.0, 1.0, 1, 1, 0)
		AttachEntityToEntity(radioProp, PlayerPedId(), GetPedBoneIndex(PlayerPedId(), 57005), 0.14, 0.01, -0.02, 110.0, 120.0, -15.0, 1, 0, 0, 0, 2, 1)
  else
    StopAnimTask(PlayerPedId(), "cellphone@", "cellphone_text_read_base", 1.0)
    ClearPedTasks(PlayerPedId())
    if radioProp ~= nil then
      DeleteEntity(radioProp)
      radioProp = nil
    end
  end
end

function PrintDebug(...)
  if Config.Debug then
    print(...)
  end
end