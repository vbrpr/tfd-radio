Framework = exports[Config.Framework.Core]:GetCoreObject()

PlayerRadioData = {
  radio = "",
  power = false,
  channel = 0,
  frequency = "",
  opened = "",
  micClicks = {}
}

Citizen.CreateThread(function()
  exports[Config.Framework.VoiceResourceName]:setVoiceProperty("micClicks", "false") -- Disable mic clicks in pma-voice, we handle them here

  while true do
    Wait(5000)
    if PlayerRadioData.radio ~= "" and not exports[Config.Framework.Inventory]:HasItem(PlayerRadioData.radio) then
      PrintDebug("Radio removed from inventory, removing from player data")
      powerOff()
      PlayerRadioData.radio = ""
    end
  end
end)

RegisterNetEvent("tfd-radio:client:useRadio", function(item)
  PrintDebug("Use radio event triggered: " .. item)
  if not isValidRadio(item) then
    print("Caught invalid radio item: " .. item)
    return
  end
  toggleAnimation(true)
  PlayerRadioData.opened = item
  SetNuiFocus(true, true)
  SendNUIMessage({
    data = "SHOW_RADIO",
    radio = getRadio(item).NUIRadio
  })
end)

RegisterNUICallback("up", function()
  PrintDebug("Up button pressed")
  handleChannel(1)
end)

RegisterNUICallback("down", function()
  PrintDebug("Down button pressed")
  handleChannel(-1)
end)

RegisterNUICallback("power", function()
  PrintDebug("Power button pressed")
  togglePower()
end)

RegisterNUICallback("close", function()
  PrintDebug("Close event from NUI received")
  SetNuiFocus(false, false)
  SendNUIMessage({
    data = "CLOSE_RADIO"
  })
  toggleAnimation(false)
end)

function isValidRadio(item)
  return getRadio(item) ~= nil
end

function getRadio(item)
  for _, v in pairs(Config.Radios) do
    if v.Item == item then
      return v
    end
  end
  return nil
end

function powerOff()
  removeCurrentChannel()
  PlayerRadioData.channel = 1
  PlayerRadioData.power = false
  SendNUIMessage({
    data = "SET_POWER",
    radio = getRadio(PlayerRadioData.opened).NUIRadio,
    state = false
  })
end

function powerOn()
  PlayerRadioData.power = true
  -- Configure mic clicks if set
  if getRadio(PlayerRadioData.opened).MicClicks then
    PlayerRadioData.micClicks = getRadio(PlayerRadioData.opened).MicClicks
  else
    PlayerRadioData.micClicks = {}
  end

  setCurrentFrequency(1, getRadio(PlayerRadioData.radio).Channels[1].Frequency, getRadio(PlayerRadioData.opened).Channels[1].Name)

  SendNUIMessage({
    data = "SET_POWER",
    radio = getRadio(PlayerRadioData.opened).NUIRadio,
    channel = getRadio(PlayerRadioData.radio).Channels[PlayerRadioData.channel].name,
    state = true
  })
end

function togglePower(force)
  if force then
    powerOff()
    PlayerRadioData.radio = ""
  end

  if PlayerRadioData.power and PlayerRadioData.radio ~= PlayerRadioData.opened then
    powerOff()
  end

  PlayerRadioData.radio = PlayerRadioData.opened
  powerOn()
end

function handleChannel(inc)
  local c = getRadio(PlayerRadioData.opened).Channels
  local newChannel = PlayerRadioData.channel + inc
  if newChannel > #c then
    newChannel = 1
  elseif newChannel < 1 then
    newChannel = #c
  end
  removeCurrentFrequency()
  print("Setting current frequency to " .. c[newChannel].Frequency .. " (" .. c[newChannel].Name .. ")")
  setCurrentFrequency(newChannel, c[newChannel].Frequency, c[newChannel].Name)
end

function removeCurrentFrequency()
  PrintDebug("Removing player from radio")
  exports[Config.Framework.VoiceResourceName]:removePlayerFromRadio()
end

function setCurrentFrequency(id, frequency, name)
  PrintDebug("Adding player to radio: " .. frequency .. ", " .. name)
  exports[Config.Framework.VoiceResourceName]:addPlayerToRadio(tonumber(frequency), name)
  PlayerRadioData.channel = tonumber(id)
  PlayerRadioData.frequency = frequency
  SendNUIMessage({
    data = "SET_CHANNEL",
    channel = name,
    radio = getRadio(PlayerRadioData.opened).NUIRadio
  })
end

RegisterCommand("rchannelup", function()
  PrintDebug("Channel up command received")
  if PlayerRadioData.radio ~= "" and PlayerRadioData.power then
    SendNUIMessage({ data = "PLAY_BUTTON_SOUND" })
    handleChannel(1)
  end
end, false)
RegisterCommand("rchanneldown", function()
  PrintDebug("Channel down command received")
  if PlayerRadioData.radio ~= "" and PlayerRadioData.power then
    SendNUIMessage({ data = "PLAY_BUTTON_SOUND" })
    handleChannel(-1)
  end
end, false)
RegisterKeyMapping("rchannelup", "Radio Channel Up", "keyboard", "NUMPAD8")
RegisterKeyMapping("rchanneldown", "Radio Channel Down", "keyboard", "NUMPAD2")

function findFrequency(frequency)
  for k, v in pairs(Config.Channels) do
    for i=1, #v, 1 do
      if v[i].Frequency == frequency then
        return {
          group = k,
          data = v[i]
        }
      end
    end
  end

  return nil
end