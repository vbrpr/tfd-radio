local blip = nil
local blips = {}

RegisterCommand("radioblip", function(source, args)
  for i=1, #blips, 1 do
    RemoveBlip(blips[i])
  end

  if args[1] == nil then
    return
  end

  local x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
  local r = {}
  for i=1, #args, 1 do
    r[i] = tonumber(args[i]) + 0.0
  end

  local color = 0
  local alpha = 255

  local alphaperlayer = 75 / #r

  local i = #r

  while i > 0 do
    PrintDebug("Adding blip for radius: " .. r[i] .. " at " .. x .. ", " .. y .. ", " .. z .. ", alpha=" .. (255 - (alphaperlayer * i)))
    local blip = AddBlipForRadius(x, y, z, r[i])
    --SetBlipHighDetail(blip, true)
    SetBlipColour(blip, color)
    SetBlipAlpha(blip, math.floor(128 - (alphaperlayer * i)))
    table.insert(blips, blip)
    i = i - 1
  end
end)