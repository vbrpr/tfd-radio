Framework = exports[Config.Framework.Core]:GetCoreObject()

Citizen.CreateThread(function()
  for _, v in pairs(Config.Radios) do
    Framework.Functions.CreateUseableItem(v.Item, function(source, item)
      TriggerClientEvent("tfd-radio:client:useRadio", source, item.name)
    end)
  end
end)

Framework.Functions.CreateCallback("tfd-radio:server:getPlayerCoords", function(source, cb, id)
  local ped = GetPlayerPed(id)

  if not ped then
    cb(vector3(0,0,0))
    return
  end

  cb(GetEntityCoords(ped))
end)