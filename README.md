# tfd-radio

## QBCore Items

Example items for qbcore > shared > items.lua

```
		["floaterola"] = {["name"] = "floaterola", ["label"] = "Floaterola",  ["weight"] = 50, 		["type"] = "item", 		["image"] = "floaterola.png", 		["unique"] = false, 		["useable"] = true, 	["shouldClose"] = true, 	["combinable"] = nil, 	["description"] = "Floaterola NRP-100 is a Public Safety radio"},
		["walkie"] = {["name"] = "walkie", ["label"] = "Walk-n-Talk",  ["weight"] = 50, 		["type"] = "item", 		["image"] = "walkie.png", 		["unique"] = false, 		["useable"] = true, 	["shouldClose"] = true, 	["combinable"] = nil, 	["description"] = "Floaterola Walk-n-Talk civilian walkie"},
```

The script will register them as useable automatically

## vbrp-voice

This is a slightly modified version of pma-voice that can be found at https://gitlab.com/vbrpr/pma-voice.

## Installation

Clone git repo, edit shared/config.lua, add items to QBCore or your framework, ensure script in your server.cfg

## Testing

There is a /radioblip command that will spawn radius rings around you as you specify to aid in configuration. After running this command, you will see the radius in the pause menu.

```
/radioblip 900 1300 1700 2300
```

Run with no arguments to clear previous rings.

## License

This project is licensed under the GPL-3.0 License - see the [LICENSE](LICENSE) file for details.