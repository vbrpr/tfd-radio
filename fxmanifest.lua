fx_version 'cerulean'
game 'gta5'

shared_scripts {
  'shared/*.lua',
}

client_scripts {
  'client/*.lua',
}

server_scripts {
  'server/*.lua',
}

ui_page 'ui/index.html'
files {
  'ui/*',
}